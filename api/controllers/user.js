var util = require('util'),
ProjectModelBlogs = require('../../schemas/blogs'),
ProjectModelComments = require('../../schemas/blogsComments');
var fs = require('fs')

module.exports = {
    addBlogs:addBlogs,                         //operation id: Function name
    listingBlogs:listingBlogs,
    deleteBlogs:deleteBlogs,
    editBlogs:editBlogs,
    addBlogsComments:addBlogsComments,
    listingComments:listingComments,
    getBlogBYid:getBlogBYid,
};

//Saving blog into database api
function addBlogs(req,res){
    console.log(" req.body  =============",req.body);
    var timestamp = Date.now();
    var originalImageName = timestamp + "_" + req.files.file[0].originalname;
    var imagePath = "../swagger_backend/public/images"+ originalImageName;
    fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
        if(err) {throw err}
        console.log("Image uploaded!")
    })
    var record = new ProjectModelBlogs();
        record.name=req.swagger.params.name.value;
        record.description=req.swagger.params.description.value;
        record.file="http://localhost:10010/images"+ originalImageName;
        record.save(function (err, data) {
            console.log('in addBlogs')
            res.send({code:200,message:'Blog added successfully.', data:data});    
        });
          
    //      simple adding of record
    //   var record = new ProjectModelBlogs();
    //       record.name = req.body.name,
    //       record.description = req.body.description,
    //       console.log(record);
    //       record.save(function (err, data) {
    //           console.log('in addBlogs')
    //           res.send({code:200,message:'Blog added successfully.', data:data});    
    //       });
}

//Listing blogs
function listingBlogs(req,res){
    console.log("in listingblogs1");
    ProjectModelBlogs.find(function (err, data) {
        res.send(data);
    })
}

//Delete blogs
function deleteBlogs(req,res){
    // console.log("in delete", req.body);
    // ProjectModelBlogs.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    //     if (err) return next(err);
    //     res.json(post);
    //   });
    var id = req.swagger.params.id.value
    console.log("in delete and id: ", id);
    ProjectModelBlogs.findByIdAndRemove({_id:id}, function (err, data) {
            if (err) {throw err;}
            else {res.json(data);}           
          });
}

//Edit Blogs
function editBlogs(req,res){
    console.log("in editBlogs",res)
    var _id = req.swagger.params.id.value
    var timestamp = Date.now();
    var originalImageName = timestamp + "_" + req.files.file[0].originalname;
    var imagePath = "../public/images"+ originalImageName;
    fs.writeFile(imagePath,(req.files.file[0].buffer),function(err){
        if(err) {throw err}
        console.log("Image updated!")
    })    
    var name = req.body.name;
    var description = req.body.description;
    var file = "http://localhost:10010/images"+ originalImageName;

    ProjectModelBlogs.findByIdAndUpdate(_id,{ $set: 
        {name:name, 
        description:description,
        file:file}},    
        function (err, data) {
         if (err) {throw err;}
            else {console.log("data:", data)} 
            data.save();
            res.json({data:data,code:200})
    });
       
    // ProjectModelBlogs.update(
    //     { _id: req.body.data.id },
    //     { name: req.body.data.name, description: req.body.data.description},
    //     function (err, data) {
    //         ProjectModelBlogs.find({}, function (err, data) {
    //             var datas = {
    //                 id: id,
    //                 status: 200,
    //                 data: data
    //             }
    //             res.send(datas)
    //         })
    //    console.log("in editBlogs",res) })
}


//=====saving comments and rating on blog
function addBlogsComments(req,res){
    var record = new ProjectModelComments();
        record.comments = req.body.comments,
        record.blogId = req.body.blogId,
        record.userId = req.body.userId,
        record.rating = req.body.rating,
        record.date = Date.now(),
        console.log(record);
        record.save(function (err, data) {
            console.log('in addBlogsComments')
    res.send({code:200,message:'Blog comment added successfully.', data:data});    
          });
}

//========listing comments
function listingComments(req,res){
    // console.log("req.quert",req.query)
    console.log("in listcomments",req.swagger.params.id.value);
    var id=req.swagger.params.id.value;
    // ProjectModelComments.find(function (err, data) {
    //     res.send(data);
    // })
    ProjectModelComments.find({blogId:id})
    .populate({
        path:'blogId',
        model:'blogs'
    }).populate('userId', 'username')
    .exec(function(err,data){
        if(err){
            console.log(err)
        }else{
            console.log("data/.....",data)
            res.json(data);

        }
    })
}

function getBlogBYid(req, res){
    var _id = req.swagger.params.id.value;
    ProjectModelBlogs.findOne({_id:_id},function(err, data){
        if(err){
            console.log(err);
        }else if(data){
            res.json(data);
        }else{
            console.log(err)
        }

    })
}

