var ProjectModelCart=require('../../schemas/userCart')

module.exports = {
    addToCart:addToCart,                        //operation id: Function name
};


//==================add products to cart
function addToCart(req,res){
    var cartRecord = new ProjectModelCart();
    cartRecord.quantity = req.body.quantity;  //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
    cartRecord.productName = req.body.productName;
    cartRecord.productId = req.body.productId;
    cartRecord.productCost = req.body.productCost;
    cartRecord.totalCost = req.body.totalCost;
    // cartRecord.is_delete = false;
    cartRecord.userId = req.body.userId;
    console.log(cartRecord.userId);
    var id = cartRecord.userId;


    ProjectModelCart.findOne({ 'productId': cartRecord.productId }, function (err, data) {
        if (err) throw err;
        else if (data) {
            console.log("same product");
            console.log("Complete Product Data",data);
            data.quantity = Number(data.quantity + 1);
            data.totalCost = (cartRecord.productCost * data.quantity);
            ProjectModelCart.updateOne({ "productid": data.productId }, { $set: data }, function (err, result) {
                if (err) throw err;
                else{
                    res.json(result);

                }
                console.log("Cart updated");

              });
        
        }
        else {
            cartRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'CartDetails not Added'
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response
                    })
                    console.log('Successfully added to cart');
                    
                }

            })

        }
    })

}
