var mongoose = require('mongoose');

module.exports = mongoose.model('userCart', {
    quantity:{type: Number}, 
    productName: {type :String},
    productId:{type:String, ref:'product'},
    productCost:{type :Number},
    totalCost:{type:Number},
    userId:{type: String, ref:'users'},
});
