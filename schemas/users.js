var mongoose = require('mongoose');

module.exports = mongoose.model('users', {
    username: {type :String},
    password:{type :String},
    email : {type :String},
    role: {type :String}
});
