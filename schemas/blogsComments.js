var mongoose = require('mongoose');

module.exports = mongoose.model('blogsComments', {
    comments: {type :String},
    blogId:{type: String, ref:'blogs'},
    userId:{type: String, ref: 'users'},
    rating:{type: Number},
    date:{type: Date, default: Date.now()}
});
