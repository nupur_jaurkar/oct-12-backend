var mongoose = require('mongoose');

module.exports = mongoose.model('products', {
    productName: {type :String},
    productDescription:{type :String},
    productCost:{type :Number},
    productCategory:{type: String},
    file:{type: String},

});
